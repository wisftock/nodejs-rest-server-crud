const express = require('express');
const usuarioController = require('../controllers/usuarioController');
const router = express();

router.get('/usuario', usuarioController.List);

router.post('/usuario', usuarioController.Add);

router.put('/usuario/:id', usuarioController.Update);

router.delete('/usuario/:id', usuarioController.Delete);

module.exports = router;
