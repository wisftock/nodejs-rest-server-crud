const Usuario = require('../models');
const bcrypt = require('bcryptjs');
const _ = require('underscore');

function Add(req, res) {
  const user = new Usuario();
  const { nombre, email, password } = req.body;

  user.nombre = nombre;
  user.email = email;
  user.password = bcrypt.hashSync(password, 10);

  user.save((err, usuarioDB) => {
    if (err) {
      res
        .status(500)
        .send({ error: err, message: 'Error el usuario ya existe' });
    } else {
      if (!usuarioDB) {
        res.status(404).send({ message: 'Error al registrar usuario' });
      } else {
        res
          .status(200)
          .json({ user: usuarioDB, message: 'Usuario agregado correctamente' });
      }
    }
  });
}

function List(req, res) {
  // paginacion
  let paginacion = req.query.paginacion || 0;
  paginacion = Number(paginacion);
  // limite
  let limite = req.query.limite || 5;
  limite = Number(limite);

  Usuario.find({ estado: true }, 'nombre email role estado google img')
    .skip(paginacion)
    .limit(limite)
    .exec((err, usuarioDB) => {
      // console.log(usuarioDB);
      if (err) {
        return res
          .status(400)
          .send({ error: err, message: 'Error al listar usuarios' });
      }
      Usuario.countDocuments({ estado: true }, (err, conteo) => {
        res.status(200).json({ usuarioDB, cuantos: conteo });
      });
      // res.status(200).json(usuarioDB);
    });
}

function Update(req, res) {
  const body = _.pick(req.body, ['nombre', 'email', 'img', 'role', 'estado']);
  Usuario.findByIdAndUpdate(
    req.params.id,
    body,
    { new: true, runValidators: true },
    (err, usuarioDB) => {
      if (err) {
        return res
          .status(400)
          .json({ error: err, message: 'Datos no actualizados' });
      } else {
        if (!usuarioDB) {
          return res
            .status(400)
            .json({ error: err, message: 'Datos no actualizados' });
        } else {
          res.status(200).json({
            usuarioDB,
            message: 'Datos actualizados correctamente',
          });
        }
      }
    }
  );
}

function Delete(req, res) {
  let cambiarEstado = {
    estado: false,
  };
  Usuario.findByIdAndUpdate(
    req.params.id,
    cambiarEstado,
    { new: true },
    (err, usuarioDB) => {
      if (err) {
        res.status(400).send({ error: err, message: 'Usuario no eliminado' });
      } else {
        if (!usuarioDB) {
          res
            .status(404)
            .send({ message: 'No se encontro usuario a eliminar' });
        } else {
          res.status(200).json({
            usuarioDB,
            message: 'Usuario eliminado',
          });
        }
      }
    }
  );
}

// function Delete(req, res) {
//   Usuario.findByIdAndRemove(req.params.id, (err, usuarioDB) => {
//     if (err) {
//       res.status(400).send({ error: err, message: 'Usuario no eliminado' });
//     } else {
//       if (!usuarioDB) {
//         res.status(404).send({ message: 'No se encontro usuario a eliminar' });
//       } else {
//         res.status(200).json({
//           usuarioDB,
//           message: 'Usuario eliminado',
//         });
//       }
//     }
//   });
// }

module.exports = {
  Add,
  Update,
  List,
  Delete,
};
